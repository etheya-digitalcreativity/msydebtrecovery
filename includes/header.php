<!-- Masthead -->
        <section class="row top">
            <header>

                <section class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                    <a href="{{ siteUrl }}"><img src="/assets/img/msy-logo.png" class="img-responsive" /></a>
                </section>

                <section class="col-xs-12 col-sm-8 col-md-8 col-lg-7 pull-right text-right contacttop">

                        <section class="row topaddress">
                            <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <address>
                                    <p><i class="icon-phone"></i> 01904 655410</p>
                                </address>
                            </section>
                        </section>
                        <section class="row">
                            <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p>"When you work with a member of the CSA you can rest assured that the service you will receive will respect your company’s position and reputation and you can be certain that quality and professionalism is upheld at all times"</p>
                                <p>&mdash; CSA quote</p>
                            </section>
                        </section>

                </section>
            </header>
        </section>

        <!-- Navigation -->
        <section class="row">
            <nav class="navbar navbar-default" role="navigation">
                <section class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                </section>
                <section class="collapse navbar-collapse navbar-ex1-collapse">

                <p class="navbar-text pull-right">Today is {{ now.format('M j, Y') }}</p>
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="{{ siteUrl }}">Home</a></li>
                        <li><a href="#about">About us</a></li>
                        <li><a href="#contact">Get in touch</a></li>
                    </ul>
                 </section>
            </nav>
        </section>