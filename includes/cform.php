
{% macro errorList(errors) %}
    {% if errors %}
    <div class="alert alert-danger">
        <ul class="errors">
            {% for error in errors %}
                <li>{{ error }}</li>
            {% endfor %}
        </ul>
    </div>
    {% endif %}
{% endmacro %}

<form method="post" action="" accept-charset="UTF-8" role="form">
    <fieldset>


    <input type="hidden" name="action" value="contactForm/sendMessage">
    <input type="hidden" name="successRedirectUrl" value="success">


                        <section class="row">
                            <section class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                <section class="form-group">
                                    <label for="title">Mr/Mrs/Ms/Miss/Other: <em>*</em></label>
                                    <input id="title" type="text" name="message[title]" value="" placeholder="Mr/Mrs/Ms/Miss/Other" class="form-control">
                                </section>
                                <section class="form-group">
                                    <label for="fromName">Name: <em>*</em></label>
                                    <input id="fromName" type="text" name="fromName" placeholder="your name" class="form-control" value="{% if message is defined %}{{ message.fromName }}{% endif %}">
                                    {% if message is defined %}
                                        {{ _self.errorList(message.getErrors('fromName')) }}
                                    {% endif %}
                                </section>
                                <section class="form-group">
                                    <label for="company">Company Name (If Applicable): </label>
                                    <input id="company" type="text" name="message[company]" placeholder="company name" class="form-control" value="">

                                </section>
                                <section class="form-group">
                                    <label for="fromEmail">Email Address: <em>*</em></label>
                                    <input id="fromEmail" type="email" placeholder="email address" class="form-control" name="fromEmail" value="{% if message is defined %}{{ message.fromEmail }}{% endif %}">
                                    {% if message is defined %}
                                        {{ _self.errorList(message.getErrors('fromEmail')) }}
                                    {% endif %}
                                </section>

                                <section class="form-group">
                                    <label for="telephone">Telephone: </label>
                                    <input id="telephone" type="number" placeholder="your telephone" class="form-control" name="message[telephone]" value="">

                                </section>
                                <section class="form-group">
                                    <label for="postal">Postal Address: </label>
                                    <input id="postal" type="text" placeholder="Postal Address" class="form-control" name="message[postal]" value="">

                                </section>
                            </section>
                            <section class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <section class="form-group">
                                    <label for="method">Preferred Contact Method (Email/Telephone/Post): </label>
                                    <input id="method" type="text" placeholder="Preferred Contact Method (Email/Telephone/Post)" class="form-control" name="message[method]" value="">

                                </section>
                                <section class="form-group">
                                    <label for="debt">Value of Debt: <em>*</em></label>
                                    <input id="debt" type="number" placeholder="Value of Debt" class="form-control" name="message[debt]" value="">

                                </section>
                                <section class="form-group">

                                    <label for="message">Details of Debt: <em>*</em></label>
                                    <textarea rows="10" id="message" name="message[body]" placeholder="details of debt" class="form-control">{% if message is defined %}{{ message.message }}{% endif %}</textarea>

                                    {% if message is defined %}
                                        {{ _self.errorList(message.getErrors('message.body')) }}
                                    {% endif %}

                                </section>
                                <section class="form-group">

                                    <button type="submit" class="btn btn-info">Send Enquiry</button>
                                </section>
                            </se
                            ction>
                        </section>
                        </fieldset>
                    </form>