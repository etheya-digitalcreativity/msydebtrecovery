<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>{% if title is defined %}{{ title }} - {% endif %}{{ siteName }}</title>
    <link rel="home" href="{{ siteUrl }}" />
    <meta name="description" content="High-quality products and services, all information about the company and purchase options ">
    <meta name="keywords" content="business,company,service,services,firm,corporation,partnership" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >

    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <!-- Typekit -->
    <script type="text/javascript" src="//use.typekit.net/lvt8loo.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <link rel="icon" href="{site_url}favicon.ico" type="image/x-icon">

    <!-- CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css" />
    <link rel="stylesheet" href="/assets/css/layout.css" />

    <!-- Font Awesome -->
    <script src="/assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


</head>
<body>

    <section class="container">