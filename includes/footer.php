</section><!-- End of Container -->


    <!-- Footer -->
    <section class="row">
        <footer>
            <section class="container">
                <section class="col-xs-12 col-sm-12 col-md-12 col-12">
                <p>Company Number: 08093270 | Registered Office: MSY Debt Recovery, PO BOX 811, York, YO31 6DJ |&nbsp;&copy; 2013 MSY Debt Recovery. All Rights Reserved.</p>
                <p>&copy; 2013 Company, Inc. &middot; <a href="/privacy-policy">Privacy Policy</a> &middot; <a href="/terms-and-conditions">Terms and Conditions</a> &middot; <a href="http://www.etheya-digitalcreativity.com">by Etheya Digital Creativity</a></p>

                </section>

            </section>
        </footer>
    </section>

<!-- Scripts -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="/assets/js/main.js"></script>

<!-- Tracking Code Google -->

</body>
</html>