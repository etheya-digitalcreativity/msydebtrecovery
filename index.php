{% include 'includes/metadata.html' %}
{% include 'includes/header.html' %}


        <!-- Main column row -->
        <section class="row botmarg">
            <div id="carousel-example-generic" class="carousel slide">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="/assets/img/tshirt.jpg" alt="...">

                        <div class="carousel-caption"></div>
                    </div>
                    <div class="item">
                        <img src="/assets/img/landrover.jpg" alt="...">
                        <div class="carousel-caption"></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="row grey bottpadd toppadd">
            <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <section class="row">
                    <section class="col-xs-12 sol-sm-12 col-md-12 col-lg-12">
                        <h1>Welcome to MSY Debt Recovery</h1>
                        <p class="lead">In today's economic climate it is vital for any business to maintain a steady cash flow. Without a sufficient cash flow a business can be put under unnecessary financial strain, which in turn could affect its day to day business practices.</p>
                        <p>At MSY we understand the importance of maintaining this cash flow. We specializes in the collection of business debt, so whether you&rsquo;re a Limited Company, a small business owner, or a self employed individual, MSY can offer you an efficient and cost effective debt collection solution. Our team of professional experts will aim to recover your money as quickly and efficiently as possible, while safeguarding your business relationship with the debtor. To find out more about the services offer please get in touch.</p>
                    </section>
                </section>

                <section class="row">

                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-left">
                        <h2 id="about">About MSY</h2>
                        <p>MSY is based in York, with our local knowledge and our national networking contacts we are the recovery business on your doorstep.</p>
                        <p>Over the years MSY has become very effective in the debt recovery process , primarily focusing on the collection of business debt. Clients ranging from Letting Agents, Vet's, Accountants, Equine Services, Private Estates, Cleaners and many other business sectors.</p>
                        <p>Our team of skilled advisors all live and work within the Yorkshire area. Having a hands on approach to the debt recovery process gives us the advantage on the national debt recovery firms. MSY is a member of the Credit Services Association (CSA), who’s Code of Practice is adhered to throughout the debt recovery process.</p>
                    </section>
                    <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h2>Trade Association</h2>
                        <p>CSA members are instructed to collect more than £5 billion of debt to the UK economy every year; they offer a wide range of bespoke credit services, including credit investigation, status enquiries, company searches, credit control expertise, credit insurance support and debt purchase. Membership extends from major multinational companies to smaller more locally focused businesses. </p>
                        <p>Please click on the icons below for a full guide to the rules and regulations on our debt recovery business.</p>

                        <section class="row">
                            <section class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <a href="http://www.csa-uk.com"><img src="/assets/img/csa-logo.png" class="img-responsive" /></a>
                            </section>
                            <section class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <a href="http://www.ico.org.uk/"><img src="/assets/img/ico-logo.png" class="img-responsive" /></a>
                            </section>
                        </section>
                    </section>
                </section>
            </section>
        </section>

        <section class="row topmargin toppadd text-center contactmain grey">
            <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Contact us</h2>
                <address>
                    <p>MSY Debt Recovery, PO BOX 811, York, YO31 6DJ</p>
                    <p><i class="icon-phone icon-large"></i> 01904 655410 | <i class="icon-envelope-alt icon-large"></i> <a href="mailto:enquiries@msydebtrecovery.com">enquiries@msydebtrecovery.com</a> | <i class="icon-twitter icon-large"></i> <a href="http://www.twitter.com/MSYdebtrecovery" target="_blank" >Follow us on Twitter</a> | <i class="icon-facebook icon-large"></i> <a href="https://www.facebook.com/lion.york.7" target="_blank" >Join us on Facebook</a></p>
                </address>
            </section>
        </section>

        <!-- Start of ending row -->
        <section class="row topmargin toppadd text-right grey">
            <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <blockquote>
                    <h2>What others are saying about us...</h2>
                    <i class="icon-quote-left icon-4x pull-right"></i>
                    <p>I have worked closely with MSY over the years, not only as their accountant but also as their client. i have no hesitation in recommending the services of MSY, professional, courteous and above all effective.</p>
                    <small>David Beckington <cite title="Source Title">Founding Director At Beckingtons Chartered Certified Accountants</cite></small>
                </blockquote>
            </section>
        </section>


        <!-- Start of ending row -->
        <section class="row topmargin toppadd grey">
            <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <h2 id="contact">Get in touch</h2>
                    <p>Don't hesitate, contact us now, get your hard earned money back!</p>
                    <p><em>*</em> Denotes required field</p>
                    {% include 'includes/cform.html' %}
            </section>
        </section>

{% include 'includes/footer.html' %}
